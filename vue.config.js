const PROD_PUBLIC_PATH = process.env.CI_PROJECT_NAME
	? '/' + process.env.CI_PROJECT_NAME + '/'
	: './'

module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? PROD_PUBLIC_PATH
    : '/'
}